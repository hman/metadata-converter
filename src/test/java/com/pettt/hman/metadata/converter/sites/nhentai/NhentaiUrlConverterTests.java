package com.pettt.hman.metadata.converter.sites.nhentai;

import com.pettt.hman.metadata.converter.beans.MetadataTag;
import org.junit.Test;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertTrue;

@Slf4j
public class NhentaiUrlConverterTests {

    @Test
    public void emptySearch(){
        converterHelper(new String[0]);
    }

    @Test
    public void simpleSearch(){
        converterHelper(new String[]{"searchTerm"},"searchTerm");
    }

    @Test
    public void concatSearch(){
        converterHelper(new String[]{"momoiro","daydream","-"},"momoiro-daydream");
    }

    @Test
    public void spaceSearch(){
        converterHelper(new String[]{"search","with","spaces","+"},"search with spaces");
    }

    @Test
    public void excludeSearch(){
        converterHelper(new String[]{"-exclude"},"-exclude");
    }

    @Test
    public void namespaceSearch(){
        converterHelper(new String[]{"artist%3Ahisasi"},"artist:hisasi");
    }

    @Test
    public void specialCharactersTag(){
        converterHelper(new String[]{"artist%3Adr.p"},"artist:dr.p");
    }

    @Test
    public void excludedArtistAndGroup(){
        //should produce q=-artist%3Ahello+group%3Atestgroup
        converterHelper(new String[]{"-artist%3Ahello", "group%3Atestgroup","+"},"-artist:hello","group:testgroup");
    }

    @Test
    public void complexSearch(){
        // ?q=artist%3Adr.p+-group%3Atest+momoiro+daydream+language%3Atranslated
        converterHelper(new String[]{"artist%3Adr.p","-group%3Atest","momoiro","daydream","language%3Atranslated"},
                "artist:dr.p","-group:test", "momoiro daydream", "language:translated");
    }

    private void converterHelper(String[] expectedParameters, String... parameters) {
        Collection<MetadataTag> includeTags = new ArrayList<>();
        Collection<MetadataTag> excludeTags = new ArrayList<>();

        //parse parameters
        for (String parameter : parameters) {
            if(parameter.startsWith("-")){
                parameter = parameter.substring(1);
                excludeTags.add(NhentaiUrlConverter.stringToMetadataTag(parameter));
            } else {
                includeTags.add(NhentaiUrlConverter.stringToMetadataTag(parameter));
            }
        }

        List<String> converted = NhentaiUrlConverter.convertToUrl(includeTags, excludeTags);
        //check if empty
        if(expectedParameters.length == 0){
            assertTrue(converted.isEmpty());
            return;
        }

        // nhentai only has one parameter, q
        assertTrue(converted.size() == 1);
        String url = converted.get(0);

        assertTrue(url.startsWith("q=")); //q= should always be at the beginning

        //check single parameters
        for (String expectedParameter : expectedParameters) {
            assertTrue(url.contains(expectedParameter));
        }
    }
}
