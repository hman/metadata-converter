package com.pettt.hman.metadata.converter.sites.nhentai;

import com.pettt.hman.metadata.converter.beans.MetadataBean;
import com.pettt.hman.metadata.converter.beans.MetadataTag;
import com.pettt.hman.metadata.converter.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.pettt.hman.metadata.converter.beans.ENamespace.*;
import static com.pettt.hman.metadata.converter.sites.nhentai.NHentaiSpecification.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

// DATE 24.08.2017
@Slf4j
public class NhentaiParserTests {
    private String smallGallery = "";
    private String bigGallery = "";

    @Before
    public void parseResourceGalleries() throws IOException {
        //parse small gallery
        String smallGalleryUrl = NhentaiParserTests.class.getResource("gallery116293.json").getPath();
        smallGallery = Util.readBody(new File(smallGalleryUrl));

        //parse big gallery
        String bigGalleryUrl = NhentaiParserTests.class.getResource("gallery116976.json").getPath();
        bigGallery = Util.readBody(new File(bigGalleryUrl));
    }

    @Test
    public void bigGallery() throws IOException {
        parseHelper(bigGallery,
                namespace(TAG)+":glasses",
                namespace(TAG)+":group",
                namespace(ARTIST)+":sanagi torajirou",
                namespace(PAGES)+":147",
                namespace(CATEGORY)+":manga",
                namespace(LANGUAGE)+":japanese",
                namespace(TITLE)+":[Sanagi Torajirou] Photorare",
                namespace(TITLE_ALT)+":[蛹虎次郎] フォトラレ"
        );
    }

    @Test
    public void simpleGallery() throws IOException {
        parseHelper(smallGallery,
                namespace(PARODY)+":persona 4",
                namespace(CHARACTER)+":yuu narukami",namespace(CHARACTER)+":rise kujikawa",
                namespace(TAG)+":schoolgirl uniform",
                namespace(ARTIST)+":okazaki takeshi",
                namespace(PAGES)+":26",
                namespace(CATEGORY)+":doujinshi",
                namespace(GROUP)+":hapoi-dokoro",
                namespace(TITLE)+":(C86) [Hapoi-Dokoro (Okazaki Takeshi)] Rise Sexualis 2 (Persona 4)",
                namespace(TITLE_ALT)+":(C86) [はぽい処 (岡崎武士)] リセ・セクスアリス 2 (ペルソナ4)"
        );
    }

    private void parseHelper(String galleryBody, String... expectedTags) throws IOException {
        MetadataBean galleryBean = NhentaiParser.parseJson(galleryBody);
        assertNotNull(galleryBean);

        List<MetadataTag> expectedBeans = createExpectedTags(expectedTags);
        checkEquality(galleryBean.getMetadata(), expectedBeans, false);
    }

    @SuppressWarnings("SameParameterValue") //TODO tests with full equality
    private void checkEquality(List<MetadataTag> metadata, List<MetadataTag> expectedBeans, boolean fullCheck) {
        //check metadata in expected metadata if full check enabled
        if(fullCheck) {
            for (MetadataTag tag : metadata) {
                assertTrue("Actual tag "+tag+" not contained in expected tags.",expectedBeans.contains(tag));
            }
        }

        //check expected metadata in metadata
        for (MetadataTag tag : expectedBeans) {
            assertTrue("Expected tag "+tag+" not contained in actual tags.",metadata.contains(tag));
        }
    }

    private List<MetadataTag> createExpectedTags(String ... tags){
        List<MetadataTag> beans = new ArrayList<>();
        for (String tag : tags) {
            beans.add(new MetadataTag(tag.split(":")[0], tag.split(":")[1]));
        }

        return beans;
    }
}
