package com.pettt.hman.metadata.converter.beans;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class MetadataTagTests {
    @Test
    public void equalityTest() {
        MetadataTag t1 = new MetadataTag("namespace","name");
        MetadataTag t2 = new MetadataTag("namespace","name");

        Assert.assertEquals(t1,t2);
    }

    @Test
    public void setTest() {
        Set<MetadataTag> set = new HashSet<>();

        MetadataTag t1 = new MetadataTag("namespace","name");
        MetadataTag t2 = new MetadataTag("namespace","name");

        set.add(t1);
        set.add(t2);

        Assert.assertTrue("Set insertion not working",set.size() == 1);
    }
}
