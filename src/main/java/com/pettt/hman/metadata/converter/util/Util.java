package com.pettt.hman.metadata.converter.util;

import com.pettt.hman.metadata.converter.beans.MetadataTag;
import lombok.experimental.UtilityClass;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

@UtilityClass
public class Util {
    /** Returns all tags without namespaces */
    public static List<MetadataTag> filterRawSearch(Collection<MetadataTag> parameters) {
        List<MetadataTag> tags = new ArrayList<>();
        for (MetadataTag parameter : parameters) {
            if (parameter.getNamespace().equals(MetadataTag.DEFAULT_TAG)) tags.add(parameter);
        }

        return tags;
    }

    /** Returns all tags with namespaces */
    public static List<MetadataTag> filterNamespacedSearch(Collection<MetadataTag> parameters) {
        List<MetadataTag> tags = new ArrayList<>();
        for (MetadataTag parameter : parameters) {
            if (!parameter.getNamespace().equals(MetadataTag.DEFAULT_TAG)) tags.add(parameter);
        }

        return tags;
    }

    /** Removes all namespaces unknown to a website, returns a set with all unknown namespaces found */
    public static Set<String> validateNamespaces(List<MetadataTag> namespacedSearches, String[] knownNamespaces) {
        Set<String> unknownNamespaces = new HashSet<>();
        Iterator<MetadataTag> it = namespacedSearches.iterator();
        while(it.hasNext()){
            MetadataTag tag = it.next();
            if (!Arrays.asList(knownNamespaces).contains(tag.getNamespace())) {
                unknownNamespaces.add(tag.getNamespace());
                it.remove();
            }
        }

        return unknownNamespaces;
    }

    /** Encodes a string in UTF-8 for urls */
    public static String encode(String toEncode){
        try {
            return URLEncoder.encode(toEncode,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            //UTF-8 should always be available
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /** Parses a file and returns content as a String */
    public static String readBody(File data) throws IOException {
        StringBuilder body = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(data.getAbsolutePath()))) {
            stream.forEachOrdered(body::append);
        }
        return body.toString();
    }
}
