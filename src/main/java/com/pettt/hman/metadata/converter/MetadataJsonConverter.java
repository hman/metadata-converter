package com.pettt.hman.metadata.converter;

import com.pettt.hman.metadata.converter.beans.ESupportedSites;
import com.pettt.hman.metadata.converter.beans.MetadataBean;
import com.pettt.hman.metadata.converter.sites.nhentai.NhentaiParser;
import lombok.experimental.UtilityClass;

import java.io.IOException;

/** Responsible for conversion between JSON and beans */
@SuppressWarnings("unused") //utility
@UtilityClass
public class MetadataJsonConverter {

    /** Converts a json string of a single gallery string to bean
     * */
    @Deprecated
    public static MetadataBean jsonToBean(String jsonString, ESupportedSites origin) throws IOException {
        switch (origin){
            case NHENTAI_NET:
                return NhentaiParser.parseJson(jsonString);
            case EHENTAI_ORG:
                throw new RuntimeException("Not implemented");
        }

        throw new IllegalStateException("No parser found for " + origin);
    }
}
