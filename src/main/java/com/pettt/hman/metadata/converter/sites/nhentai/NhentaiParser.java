package com.pettt.hman.metadata.converter.sites.nhentai;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pettt.hman.metadata.converter.beans.ENamespace;
import com.pettt.hman.metadata.converter.beans.MetadataBean;
import com.pettt.hman.metadata.converter.beans.MetadataTag;
import lombok.Data;
import lombok.experimental.UtilityClass;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.pettt.hman.metadata.converter.beans.ENamespace.*;
import static com.pettt.hman.metadata.converter.sites.nhentai.NHentaiSpecification.*;

@UtilityClass
public class NhentaiParser {
    private final static ObjectMapper mapper = new ObjectMapper();

    /** Parses a html body for metadata. Deprecated, will not get updated if nhentai format changes.
     * This should serve as an example implementation of how to parse HTML if necessary.
     * Will get removed in 1.0.0 release. */
    @Deprecated
    public static MetadataBean parse(String htmlBody) {
        Document doc = Jsoup.parse(htmlBody);

        List<MetadataTag> tags = new ArrayList<>();

        //extract 'Tags', 'Artists', 'Languages', 'Categories', 'Parodies'
        for (int i = 0; i < doc.select("a.tag").size(); i++) {
            String tagAndNamespace = doc.select("a.tag").get(i).attr("href");
            String namespace = tagAndNamespace.split("/")[1];
            //String tag = tagAndNamespace.split("/")[2].replace("-"," ");
            //remove count groups ( e.g. (59) )
            String tag = doc.select("a.tag").get(i).text().replaceAll("\\(.*\\)","").trim();
            tags.add(new MetadataTag(namespace,tag));
        }

        { //title & alternative title
            String mainTitle = doc.select("div#info-block").select("h1").html();
            tags.add(new MetadataTag(namespace(TITLE),mainTitle));

            if(!doc.select("div#info-block").select("h2").html().isEmpty()){
                String altTitle = doc.select("div#info-block").select("h2").html();
                tags.add(new MetadataTag(namespace(TITLE_ALT),altTitle));
            }
        }

        { //thumbnail url
            String thumbnailUrl = "https:"+doc.select("div#bigcontainer").select("div#cover").select("img").attr("src");
            tags.add(new MetadataTag(namespace(THUMBNAIL),thumbnailUrl));
        }

        { //page number
            String pages = doc.select("div#info-block").select("div").get(9).html().split(" ")[0];
            tags.add(new MetadataTag(namespace(PAGES),pages));
        }

        { //date
            String date = htmlBody.substring(htmlBody.indexOf("time datetime"));
            date = date.substring(0, date.indexOf("/time"));
            date = date.substring(15);
            date = date.split(">")[0];
            date = date.substring(0, date.length() - 14);

            tags.add(new MetadataTag(namespace(UPLOADED),date));
        }

        return new MetadataBean(tags);
    }

    //TODO pretty title
    @SuppressWarnings("unchecked")
    public static MetadataBean parseJson(String jsonString) throws IOException {
        NhentaiJson nhentaiJson = mapper.readValue(jsonString, NhentaiJson.class);
        List<MetadataTag> tags = new ArrayList<>();

        tags.add(new MetadataTag(namespace(TITLE),nhentaiJson.title.get("english")));
        tags.add(new MetadataTag(namespace(TITLE_ALT),nhentaiJson.title.get("japanese")));
        tags.add(new MetadataTag(namespace(TITLE_ALT),nhentaiJson.title.get("pretty")));
        tags.add(new MetadataTag(namespace(UPLOADED),String.valueOf(nhentaiJson.upload_date)));
        tags.add(new MetadataTag(namespace(GROUP),String.valueOf(nhentaiJson.scanlator)));
        tags.add(new MetadataTag(namespace(PAGES),String.valueOf(nhentaiJson.num_pages)));
        tags.add(new MetadataTag(namespace(ENamespace.NHENTAI_ID),String.valueOf(nhentaiJson.id)));
        tags.add(new MetadataTag(namespace(ENamespace.NHENTAI_MEDIA_ID),String.valueOf(nhentaiJson.media_id)));

        //tags
        for (Object tagMap : ((List) nhentaiJson.tags)) {
            Map<String, String> tag = (Map<String, String>) tagMap;
            String namespace = tag.get("type");
            String name = tag.get("name");

            tags.add(new MetadataTag(namespace,name));
        }

        return new MetadataBean(tags);
    }


    @Data
    class NhentaiJson{
        Integer upload_date;
        Integer num_favorites;
        String media_id;
        Map<String,String> title;
        Integer id;
        Integer num_pages;
        String scanlator;

        Object tags;
    }
}