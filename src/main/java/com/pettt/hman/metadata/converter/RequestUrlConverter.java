package com.pettt.hman.metadata.converter;

import com.pettt.hman.metadata.converter.beans.MetadataTag;
import com.pettt.hman.metadata.converter.beans.ESupportedSites;
import com.pettt.hman.metadata.converter.sites.nhentai.NhentaiUrlConverter;
import lombok.experimental.UtilityClass;

import java.util.Collection;
import java.util.List;

@SuppressWarnings("unused") //utility
@UtilityClass
public class RequestUrlConverter {
    /** Converts parameters to URL requests which are accepted by the sites */
    public static List<String> parametersToUrlRequest
    (Collection<MetadataTag> includeParameters, Collection<MetadataTag> excludeParameters, ESupportedSites origin) {
        switch (origin) {
            case NHENTAI_NET:
                return NhentaiUrlConverter.convertToUrl(includeParameters, excludeParameters);
            case EHENTAI_ORG:
                break;
        }

        throw new IllegalStateException("No url converter found for " + origin);
    }
}
