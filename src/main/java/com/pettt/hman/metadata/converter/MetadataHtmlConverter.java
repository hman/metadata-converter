package com.pettt.hman.metadata.converter;

import com.pettt.hman.metadata.converter.beans.MetadataBean;
import com.pettt.hman.metadata.converter.beans.ESupportedSites;
import com.pettt.hman.metadata.converter.sites.nhentai.NhentaiParser;
import lombok.experimental.UtilityClass;

/** Responsible for conversion between HTML body text and beans */
@SuppressWarnings("unused") //utility
@UtilityClass
public class MetadataHtmlConverter {

    /** Converts a html body of a single gallery string to beans
     * <p>
     * Deprecated: Use jsonToBean of MetadataJsonConverter instead if possible
     * */
    @Deprecated
    public static MetadataBean htmlToBean(String htmlBody, ESupportedSites origin) {
        switch (origin){
            case NHENTAI_NET:
                return NhentaiParser.parse(htmlBody);
            case EHENTAI_ORG:
                throw new RuntimeException("Not implemented");
        }

        throw new IllegalStateException("No parser found for " + origin);
    }
}