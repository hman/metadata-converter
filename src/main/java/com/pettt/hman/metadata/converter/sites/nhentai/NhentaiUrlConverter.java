package com.pettt.hman.metadata.converter.sites.nhentai;

import com.pettt.hman.metadata.converter.beans.MetadataTag;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

import static com.pettt.hman.metadata.converter.sites.nhentai.NHentaiSpecification.KNOWN_NAMESPACES;
import static com.pettt.hman.metadata.converter.sites.nhentai.NHentaiSpecification.NAMESPACE_DELIMITER;
import static com.pettt.hman.metadata.converter.sites.nhentai.NHentaiSpecification.NAMESPACE_DELIMITER_URL;
import static com.pettt.hman.metadata.converter.util.Util.*;

@SuppressWarnings({"unused", "WeakerAccess"}) //utility
@Slf4j
@UtilityClass
public class NhentaiUrlConverter {

    /**
     * Returns a list of parameters which nhentai can understand, non-exact tag search.
     *
     * Current format of nhentai is:
     *
     * q=[search][+search]*
     *
     * [search]     = [raw] | [tag] | [exactTag]
     * [tag]        = [-]?[namespace]:[name]
     * [exactTag]   = [-]?[namespace]:"[name]"
     */
    public static List<String> convertToUrl(Collection<MetadataTag> includeParameters, Collection<MetadataTag> excludeParameters) {
        List<String> parameters = new ArrayList<>();

        List<MetadataTag> rawSearches = filterRawSearch(includeParameters);
        List<MetadataTag> rawSearchesExclude = filterRawSearch(excludeParameters);

        List<MetadataTag> namespacedSearches = filterNamespacedSearch(includeParameters);
        List<MetadataTag> namespacedSearchesExclude = filterNamespacedSearch(excludeParameters);

        if(rawSearches.size() > 1 || rawSearchesExclude.size() > 1)
            log.warn("Found multiple raw search parameters ({}). " +
                    "Using simple concatenation, " +
                    "try providing only one raw search parameter for no ambiguity.",
                            (rawSearches.size() > 1 ?  rawSearches.size() : 0) + //sum ambiguous raw searches
                            (rawSearchesExclude.size() > 1 ? rawSearchesExclude.size(): 0));

        //ignore unknown namespaces, log warning if some are found
        Set<String> unknownNamespaces = new HashSet<>();
        unknownNamespaces.addAll(validateNamespaces(namespacedSearches, KNOWN_NAMESPACES));
        unknownNamespaces.addAll(validateNamespaces(namespacedSearchesExclude, KNOWN_NAMESPACES));
        if(!unknownNamespaces.isEmpty()) //user should not use namespaces not supported by the site, warning
            log.warn("Ignoring tags with {} unknown namespaced tags:",unknownNamespaces.size(),unknownNamespaces);

        //check if empty request
        if(rawSearches.isEmpty() && rawSearchesExclude.isEmpty()
                && namespacedSearches.isEmpty() && namespacedSearchesExclude.isEmpty()){
            return parameters;
        }

        //nhentai accepts only a single parameter, q=...
        StringBuilder search = new StringBuilder("q=");

        //concatenate raw searches
        for (MetadataTag rawSearch : rawSearches) {
            search.append(encode(rawSearch.getTag()))
                    .append("+");
        }
        for (MetadataTag rawSearchExclude : rawSearchesExclude) {
            search.append("-")
                    .append(encode(rawSearchExclude.getTag()))
                    .append("+");
        }

        //add namespaced searched
        for (MetadataTag namespacedSearch : namespacedSearches) {
            search.append(encode(namespacedSearch.getNamespace()))
                    .append(NAMESPACE_DELIMITER_URL)
                    .append(encode(namespacedSearch.getTag())).append("+");
        }

        for (MetadataTag namespacedSearch : namespacedSearchesExclude) {
            search.append("-").append(encode(namespacedSearch.getNamespace()))
                    .append(NAMESPACE_DELIMITER_URL)
                    .append(encode(namespacedSearch.getTag())).append("+");
        }


        //remove last +
        search = new StringBuilder(search.substring(0, search.length() - 1));
        parameters.add(search.toString());
        return parameters;
    }

    /** Tries to convert a string to a metadata tag according to nhentai specifications, non-exact search */
    public MetadataTag stringToMetadataTag(String tag){
        if(tag.contains(NAMESPACE_DELIMITER)){
            return new MetadataTag(tag.split(NAMESPACE_DELIMITER)[0], tag.split(NAMESPACE_DELIMITER)[1]);
        } else {
            return new MetadataTag(tag);
        }
    }
}
