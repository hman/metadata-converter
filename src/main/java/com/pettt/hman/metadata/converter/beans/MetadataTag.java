package com.pettt.hman.metadata.converter.beans;

import lombok.Getter;
import lombok.NonNull;

public class MetadataTag {
    public static final String DEFAULT_TAG = "tag";
    private @Getter @NonNull final String namespace;
    private @Getter @NonNull final String tag;

    public MetadataTag(String tag){
        this.namespace = DEFAULT_TAG;
        this.tag = tag;
    }

    public MetadataTag(String namespace, String tag){
        this.namespace = namespace;
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetadataTag)) return false;

        MetadataTag that = (MetadataTag) o;

        boolean flag1 = (namespace.equals(that.namespace));
        boolean flag2 = (tag.equals(that.tag));

        return flag1 && flag2;
    }

    @Override
    public int hashCode() {
        int result = 31;
        result = 31 * result + namespace.hashCode();
        result = 31 * result + tag.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "["+namespace+ ":" + tag+ "]";
    }
}
