package com.pettt.hman.metadata.converter.beans;

public enum ENamespace {
    //non-relational, should not be used in a DB tags table as the metadata is specific to a gallery
    TITLE(false), TITLE_ALT(false), PAGES(false), UPLOADED(false), THUMBNAIL(false),

    //non-relational, site specific information (e.g. ids)
    NHENTAI_ID(false), NHENTAI_MEDIA_ID(false), EHENTAI(false),

    //relational, can be used in a DB tags table
    CATEGORY(true), ARTIST(true), GROUP(true), PARODY(true), CHARACTER(true), LANGUAGE(true), TAG(true);

    public final boolean relational;

    ENamespace(boolean relational) {
        this.relational = relational;
    }
}
