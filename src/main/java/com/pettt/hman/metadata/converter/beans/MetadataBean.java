package com.pettt.hman.metadata.converter.beans;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.pettt.hman.metadata.converter.sites.nhentai.NHentaiSpecification.getRelationalNamespaces;

@SuppressWarnings({"unused", "WeakerAccess"}) //library functions
@EqualsAndHashCode
public class MetadataBean {
    private @NonNull @Getter final List<MetadataTag> metadata = new ArrayList<>();

    public MetadataBean(Collection<? extends MetadataTag> tags){
        metadata.addAll(tags);
    }

    public List<MetadataTag> findAllRelationalMetadata(){
        return findAllByNamespaceIsIn(getRelationalNamespaces());
    }

    public List<MetadataTag> findAllByNamespaceIsIn(Collection<String> namespaces){
        List<MetadataTag> tags = new ArrayList<>();
        for (MetadataTag metadatum : metadata) {
            if(!namespaces.contains(metadatum.getNamespace())) continue;

            tags.add(metadatum);
        }

        return tags;
    }

    public List<MetadataTag> findAllByNamespaceIsNotIn(Collection<String> namespaces){
        List<MetadataTag> tags = new ArrayList<>();
        for (MetadataTag metadatum : metadata) {
            if(namespaces.contains(metadatum.getNamespace())) continue;

            tags.add(metadatum);
        }

        return tags;
    }

    public List<MetadataTag> findAll(){
        return new ArrayList<>(metadata);
    }

    public MetadataTag findFirstByNamespace(String namespace){
        for (MetadataTag tag : metadata) if(tag.getNamespace().equals(namespace)) return tag;

        return null;
    }

    public List<MetadataTag> findAllByNamespace(String namespace){
        List<MetadataTag> result = new ArrayList<>(metadata.size());
        for (MetadataTag tag : metadata) if(tag.getNamespace().equals(namespace)) result.add(tag);

        return result;
    }

    public MetadataTag findFirstByTag(String tag){
        for (MetadataTag metadataTag : metadata) if(metadataTag.getTag().equals(tag)) return metadataTag;

        return null;
    }

    public List<MetadataTag> findAllByTag(String tag){
        List<MetadataTag> result = new ArrayList<>(metadata.size());
        for (MetadataTag metadataTag : metadata) if(metadataTag.getTag().equals(tag)) result.add(metadataTag);

        return result;
    }

    public MetadataTag findFirstByNamespaceAndTag(String namespace, String tag){
        for (MetadataTag metadataTag : metadata)
            if(metadataTag.getTag().equals(tag) && metadataTag.getNamespace().equals(namespace))
                return metadataTag;

        return null;
    }

    public List<MetadataTag> findAllByNamespaceAndTag(String namespace, String tag){
        List<MetadataTag> result = new ArrayList<>(metadata.size());
        for (MetadataTag metadataTag : metadata)
            if(metadataTag.getTag().equals(tag) && metadataTag.getNamespace().equals(namespace))
                result.add(metadataTag);

        return result;
    }

}
