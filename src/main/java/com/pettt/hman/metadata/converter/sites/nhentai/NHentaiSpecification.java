package com.pettt.hman.metadata.converter.sites.nhentai;

import com.pettt.hman.metadata.converter.beans.ENamespace;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pettt.hman.metadata.converter.util.Util.encode;

@SuppressWarnings({"WeakerAccess", "unused"}) //utility
@UtilityClass
@Slf4j
public class NHentaiSpecification {
    public final static String EXCLUDE_SYMBOL      = "-";   //eg -group:nanakorobi
    public final static String EXACT_SEARCH_SYMBOL = "\"";  //eg group:"nanakorobi-yao"
    public final static String JOIN_SYMBOL         = "-";   //eg group:nanakarobi-yao
    public final static String NAMESPACE_DELIMITER = ":";   //eg group:testgroup
    public final static String NAMESPACE_DELIMITER_URL = encode(":");  // : needs to be encoded

    //map namespaces for nhentai
    public final static String[] KNOWN_NAMESPACES
            = new String[]{"category","artist","group","parody","character","language"};
    public final static Map<ENamespace, String> NAMESPACE_MAP = new HashMap<>();
    static {
        NAMESPACE_MAP.put(ENamespace.CATEGORY, KNOWN_NAMESPACES[0]);
        NAMESPACE_MAP.put(ENamespace.ARTIST, KNOWN_NAMESPACES[1]);
        NAMESPACE_MAP.put(ENamespace.GROUP, KNOWN_NAMESPACES[2]);
        NAMESPACE_MAP.put(ENamespace.PARODY, KNOWN_NAMESPACES[3]);
        NAMESPACE_MAP.put(ENamespace.CHARACTER, KNOWN_NAMESPACES[4]);
        NAMESPACE_MAP.put(ENamespace.LANGUAGE, KNOWN_NAMESPACES[5]);

        //non-standard nhen namespaces
        NAMESPACE_MAP.put(ENamespace.TAG,"tag");
        NAMESPACE_MAP.put(ENamespace.TITLE,"title");
        NAMESPACE_MAP.put(ENamespace.TITLE_ALT,"title_alt");
        NAMESPACE_MAP.put(ENamespace.PAGES,"pages");
        NAMESPACE_MAP.put(ENamespace.UPLOADED,"uploaded");
        NAMESPACE_MAP.put(ENamespace.THUMBNAIL,"thumbnail");

        NAMESPACE_MAP.put(ENamespace.NHENTAI_ID,"nhentai_id");
        NAMESPACE_MAP.put(ENamespace.NHENTAI_MEDIA_ID,"nhentai_media_id");
    }

    public String namespace(ENamespace namespace){
        String tag = NAMESPACE_MAP.get(namespace);
        if(tag == null) {
            log.warn("Could not find tag mapping for namespace '{}'. Returning default tag namespace.", namespace);
            return "tag";
        }
        return tag;
    }

    public static List<String> getRelationalNamespaces(){
        List<String> namespaces = new ArrayList<>();

        for (ENamespace namespace : ENamespace.values()) {
            if(namespace.relational){
                namespaces.add(NHentaiSpecification.namespace(namespace));
            }
        }

        return namespaces;
    }
}
