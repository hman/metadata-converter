# Metadata Converter v0.2.x

This module can be used for site-specific conversion. Can also be used as a library.

* parsing scraped html files and generating java objects (deprecated, will get removed in v1.0.0 release)
* parsing scraped json files and generating java objects
* generating urls for known sites


## Changelog

### v0.2.x

* nhentai json parser

### v0.1.x

* nhentai html parser
* nhentai url converter


## Scheduled 

* e(x)hentai json parser
* e(x)hentai url converter
